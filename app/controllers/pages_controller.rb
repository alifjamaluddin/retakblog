class PagesController < ApplicationController
  def landing
  	@articles = Article.order(created_at: :desc).limit(3)
  end

  def about
  end

  def contact
  end
end


